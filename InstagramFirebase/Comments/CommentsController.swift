//
//  CommentsController.swift
//  InstagramFirebase
//
//  Created by Ling on 21.10.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit
import Firebase

class CommentsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var post: Post? {
        didSet {
            print("Set comments controller to: ", self.post?.caption ?? "")
        }
    }
    
    let cellId = "commentCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Comments"
        collectionView.backgroundColor = .white
        collectionView.alwaysBounceVertical = true
        collectionView?.keyboardDismissMode = .interactive
        
        collectionView.register(CommentCell.self, forCellWithReuseIdentifier: cellId)
        
        fetchComments()
    }
    
    var comments = [Comment]()
    fileprivate func fetchComments() {
        guard let postId = self.post?.id else { return }
        let ref = Database.database().reference().child("comments").child(postId)
        
        ref.observe(.childAdded) { (snapshot) in

            guard let dictionary = snapshot.value as? [String: Any], let uid = dictionary["user"] as? String else { return }
            
            
            Database.fetchUserWithUid(uid: uid, completion: { (user) -> (Void) in
                
                let comment = Comment(user: user, dictionary: dictionary)
                self.comments.append(comment)
                self.collectionView?.reloadData()
            })
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return comments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        let dummyCell = CommentCell(frame: frame)
        dummyCell.comment = comments[indexPath.item]
        dummyCell.layoutIfNeeded()
        let targetSize = CGSize(width: view.frame.width, height: 1000)
        let estimatedSize = dummyCell.systemLayoutSizeFitting(targetSize)
        
        let height = max(40 + 8 + 8, estimatedSize.height)
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? CommentCell {
            cell.comment = comments[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    lazy var containerView: UIView = {
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
        let commentInputAccessoryView = CommentInputAccessoryView(frame: frame)
        
        commentInputAccessoryView.delegate = self
        
        return commentInputAccessoryView
    }()

    override var inputAccessoryView: UIView? {
        get {
            return containerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
}

extension CommentsController: CommentInputAccessoryViewDelegate {
    func didSubmit(for comment: String) {
        print("Inserting comment:", comment)

        guard let postId = post?.id, let userId = Auth.auth().currentUser?.uid else { return }
        let values = ["text": comment,
                      "creationDate": Date().timeIntervalSince1970,
                      "user": userId] as [String: Any]
        let ref = Database.database().reference().child("comments").child(postId).childByAutoId()

        ref.updateChildValues(values) { (err, ref) in
            if let err = err {
                print("Failed to insert comment:", err)
                return
            }

            print("Successfully inserted comment.")
        }
    }
}
