//
//  MultiLineTextField.swift
//  InstagramFirebase
//
//  Created by Ling on 28.10.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit

class MultiLineTextField: UITextView {
    
    var placeholder: String? {
        didSet {
            self.text = placeholder
        }
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
