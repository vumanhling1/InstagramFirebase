//
//  CommentInputeAccessoryView.swift
//  InstagramFirebase
//
//  Created by Ling on 27.10.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit
import UITextView_Placeholder

protocol CommentInputAccessoryViewDelegate {
    func didSubmit(for comment: String)
}

class CommentInputAccessoryView: UIView {
    
    var delegate: CommentInputAccessoryViewDelegate?
    
    let textInputView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.placeholder = "Enter Comment"
        textView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        textView.backgroundColor = UIColor.rgb(red: 240, green: 240, blue: 240)
        textView.layer.cornerRadius = 30 / 2
        textView.isScrollEnabled = false
        //textField.addTarget(self, action: #selector(textFieldDidChanged), for: .editingChanged)
        return textView
    }()
    
    lazy var submitButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitle("Submit", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        return button
    }()
    
    @objc func handleSubmit() {
        guard let submitText = textInputView.text else { return }
        delegate?.didSubmit(for: submitText)
        setEmptyState()
    }
    
    func setEmptyState() {
        textInputView.text = ""
        submitButton.isEnabled = false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        autoresizingMask = .flexibleHeight
        textInputView.delegate = self
        
        backgroundColor = .white
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.rgb(red: 200, green: 200, blue: 200).cgColor
        
        addSubview(submitButton)
        submitButton.anchor(top: topAnchor, left: nil, bottom: safeAreaLayoutGuide.bottomAnchor, right: rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 8, paddingRight: 12, width: 50, height: 0)
        
        addSubview(textInputView)
        textInputView.anchor(top: topAnchor, left: leftAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: submitButton.leftAnchor, paddingTop: 8, paddingLeft: 12, paddingBottom: 8, paddingRight: 12, width: 0, height: 0)
    }
    
    override var intrinsicContentSize: CGSize {
        return .zero
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CommentInputAccessoryView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if let text = self.textInputView.text, text.isEmpty {
            submitButton.isEnabled = false
        } else {
            submitButton.isEnabled = true
        }
    }
}
