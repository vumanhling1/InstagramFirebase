//
//  FireBaseUtils.swift
//  InstagramFirebase
//
//  Created by Ling on 18.09.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import Firebase
import Foundation

extension Database {
    static func fetchUserWithUid(uid: String, completion: @escaping (User) -> (Void)) {
        Database.database().reference().child("users").child(uid).observe(.value, with: { (snapshot) in
            guard let userDictionary = snapshot.value as? [String: Any] else { return }
            let user = User(uid: uid, dictionary: userDictionary)
            completion(user)
        }) { (err) in
            print("Failed to fetch user for posts", err)
        }
    }
}
