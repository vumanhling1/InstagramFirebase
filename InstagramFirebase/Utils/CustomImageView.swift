//
//  CustomImageView.swift
//  InstagramFirebase
//
//  Created by Ling on 14.09.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {
    var lastURLUsedToLoadImages: String?
    
    func loadImage(url: String) {
        guard let url = URL(string: url) else { return }
        lastURLUsedToLoadImages = url.absoluteString
        
        self.image = nil
        
        if let cachedImage = imageCache[url.absoluteString] {
            self.image = cachedImage
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if let err = err {
                print("Failed to fetch post image: ",err)
            }
            
            if url.absoluteString != self.lastURLUsedToLoadImages {
                return
            }
            
            guard let imageData = data else { return }
            let photoImage = UIImage(data: imageData)
            
            imageCache[url.absoluteString] = photoImage
            
            DispatchQueue.main.async {
                self.image = photoImage
            }
        }.resume()
    }
}
