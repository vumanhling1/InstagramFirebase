//
//  Extensions.swift
//  InstagramFirebase
//
//  Created by Ling on 14.08.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    enum ProjectColors {
        case activeButtonState
        case inactiveButtonState
    }
    
    static func mainColors(_ color: ProjectColors = .inactiveButtonState) -> UIColor {
        switch color {
        case .activeButtonState:
            return UIColor.rgb(red: 17, green: 154, blue: 237)
        case .inactiveButtonState:
            return UIColor(white: 0, alpha: 0.2)
        }
    }
    
}

extension NSMutableAttributedString {
    func appendSlim(text: String, size: CGFloat, color: UIColor = UIColor.lightGray) {
        append(NSAttributedString(string: text, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: size), NSAttributedString.Key.foregroundColor: color]))
    }
    
    func appendBold(text: String, size: CGFloat, color: UIColor = UIColor.darkGray) {
        append(NSAttributedString(string: text, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: size), NSAttributedString.Key.foregroundColor: color]))
    }
    
}

extension MainTabBarController: UITabBarControllerDelegate  {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let index = viewControllers?.index(of: viewController)
        if index == 2 {
            let layout = UICollectionViewFlowLayout()
            let photoSelectorController = PhotoSelectorController(collectionViewLayout: layout)
            let navigationController = UINavigationController(rootViewController: photoSelectorController)
            present(navigationController,animated: true, completion: nil)
            
            return false
        }
        return true
    }
}

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let right = right {
            self.rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if width != 0 {
            self.widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            self.heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        
    }
}

extension Date {
    func timeAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
        var timeAgo = ""
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        let month = 4 * week
        
        if secondsAgo < minute {
            timeAgo = secondsAgo > 1 ? "\(secondsAgo) second ago" : "\(secondsAgo) seconds ago"
        } else if secondsAgo < hour {
            timeAgo = (secondsAgo / minute) == 1 ? "\(secondsAgo / minute) minute ago" : "\(secondsAgo / minute) minutes ago"
        } else if secondsAgo < day {
            timeAgo = (secondsAgo / hour) == 1 ? "\(secondsAgo / hour) hour ago" : "\(secondsAgo / hour) hours ago"
        } else if secondsAgo < week {
            timeAgo = (secondsAgo / day) == 1 ? "\(secondsAgo / day) day ago" : "\(secondsAgo / day) days ago"
        } else if secondsAgo < month {
            timeAgo = (secondsAgo / week) == 1 ? "\(secondsAgo / week) week ago" : "\(secondsAgo / week) weeks ago"
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            timeAgo = formatter.string(from: Date(timeIntervalSince1970: Double(secondsAgo)))
        }
        
        return timeAgo
    }
}

extension UITextField {
    
    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding(_ padding: PaddingSide) {
        
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        
        
        switch padding {
            
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
            
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}
