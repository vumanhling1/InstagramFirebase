//
//  PushAnimator.swift
//  InstagramFirebase
//
//  Created by Ling on 24.07.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit

class PushAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var duration = 0.4
    var mode: animation = .present
    
    enum animation {
        case present
        case dismiss
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let toView = transitionContext.view(forKey: .to), let fromView = transitionContext.view(forKey: .from) else { return }
        
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        if mode == .present {
            toView.frame = CGRect(x: screenWidth, y: 0, width: screenWidth, height: screenHeight)
        } else {
            toView.frame = CGRect(x: -screenWidth, y: 0, width: screenWidth, height: screenHeight)
        }
        
        containerView.addSubview(toView)
        
        UIView.animate(withDuration: duration, animations: {
            
            if self.mode == .present {
                fromView.frame = fromView.frame.offsetBy(dx: -screenWidth, dy: 0)
                toView.frame = toView.frame.offsetBy(dx: -screenWidth, dy: 0)
            } else {
                fromView.frame = fromView.frame.offsetBy(dx: screenWidth, dy: 0)
                toView.frame = toView.frame.offsetBy(dx: screenWidth, dy: 0)
            }
            
        }, completion: { _ in
            
            transitionContext.completeTransition(true)
        })
    }
    
    
}
