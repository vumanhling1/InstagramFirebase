//
//  UserProfileHeader.swift
//  InstagramFirebase
//
//  Created by Ling on 20.08.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit
import Firebase

protocol UserProfileHeaderDelegate {
    func didChangeToListView()
    func didChangeToGridView()
}

class UserProfileHeader: UICollectionViewCell {
    
    var delegate: UserProfileHeaderDelegate?
    
    var user: User? {
        didSet {
            setupProfileImage()
            usernameLabel.text = user?.username
            setupCounter()
            setupEditFollowButton()
        }
    }
    
    fileprivate func setupEditFollowButton() {
        guard let loggedInUserId = Auth.auth().currentUser?.uid else { return }
        guard let userId = user?.uid else { return }
        
        if loggedInUserId == userId {
            editProfileButton.setTitle("Edit Profile", for: .normal)
            editProfileButton.backgroundColor = .white
            editProfileButton.setTitleColor(.black, for: .normal)
            editProfileButton.layer.borderColor = UIColor.lightGray.cgColor
        } else {
            Database.database().reference().child("following").child(loggedInUserId).child(userId).observeSingleEvent(of: .value) { (snapshot) in
                    if let isFollowing = snapshot.value as? Int, isFollowing == 1 {
                        self.editProfileButton.setTitle("Unfollow", for: .normal)
                        self.editProfileButton.backgroundColor = .white
                        self.editProfileButton.setTitleColor(.black, for: .normal)
                        self.editProfileButton.layer.borderColor = UIColor.lightGray.cgColor
                    } else {
                        self.editProfileButton.setTitle("Follow", for: .normal)
                        self.editProfileButton.backgroundColor = UIColor.rgb(red: 17, green: 154, blue: 237)
                        self.editProfileButton.setTitleColor(.white, for: .normal)
                        self.editProfileButton.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
                    }
            }
        }
    }
    
    fileprivate func setupCounter() {
        guard let userId = user?.uid else { return }
        let ref = Database.database().reference()
        
        ref.child("following").observe(.value, with: { (snapshot) in
            
            guard let users = snapshot.value as? [String: Any] else { return }
            
            var followersCount = 0
            var followingCount = 0
            
            for (user, followers) in users {
                if user == userId {
                    guard let followers = followers as? [String: Int] else { return }
                    for (_, following) in followers {
                        if (following == 1) {
                            followingCount += 1
                        }
                    }
                }
                guard let followers = followers as? [String: Int] else { return }
                for (uid, following) in followers {
                    if (uid == userId && following == 1) {
                        followersCount += 1
                    }
                }
            }
            
            DispatchQueue.main.async {
                var attributedText = NSMutableAttributedString()
                attributedText.appendBold(text: "\(followersCount)\n", size: 14)
                attributedText.appendSlim(text: "follower", size: 14)
                self.followersLabel.attributedText = attributedText
                
                attributedText = NSMutableAttributedString()
                attributedText.appendBold(text: "\(followingCount)\n", size: 14)
                attributedText.appendSlim(text: "following", size: 14)
                self.followingLabel.attributedText = attributedText
            }
        }) { (err) in
            print("Count stats failed: ", err)
        }
        
        ref.child("posts").observe(.value, with: { (snapshot) in
            guard let posts = snapshot.value as? [String: Dictionary<String, Any>] else { return }
            
            var postsCount = 0
            if let ownPosts = posts[userId] {
                for _ in ownPosts {
                    postsCount += 1
                }
            }
            
            DispatchQueue.main.async {
                let attributedText = NSMutableAttributedString()
                attributedText.appendBold(text: "\(postsCount)\n", size: 14)
                attributedText.appendSlim(text: "posts", size: 14)
                self.postsLabel.attributedText = attributedText
            }
            
        }) { (err) in
            print("Failed to count posts: ", err)
        }
    }
    
    @objc func handleEditProfileOrFollow() {
        guard let loggedInUserId = Auth.auth().currentUser?.uid else { return }
        guard let userId = user?.uid else { return }
        if loggedInUserId == userId {
            print("Handle edit")
        } else {
            editProfileButton.isEnabled = false
            let ref = Database.database().reference().child("following").child(loggedInUserId)
            ref.child(userId).observeSingleEvent(of: .value) { (snapshot) in
                
                if let isFollowing = snapshot.value as? Int, isFollowing == 1 {
                    let values = [userId: 0]
                    ref.updateChildValues(values){ (err, ref) in
                        if let err = err {
                            print("Unfollowing failed with user: ", err)
                            return
                        }
                        print("Successfully unfollowed user: ", self.user!.username)
                        self.editProfileButton.isEnabled = true
                        self.setupEditFollowButton()
                    }
                } else {
                    let values = [userId: 1]
                    ref.updateChildValues(values) { (err, ref) in
                        if let err = err {
                            print("Following failed with user: ", err)
                            return
                        }
                        print("Successfully followed user: ", self.user!.username)
                        self.editProfileButton.isEnabled = true
                        self.setupEditFollowButton()
                    }
                }
            }
        }
    }
    
    let profileImageView: CustomImageView = {
        let iv = CustomImageView()

        return iv
    }()
    
    lazy var gridButton: UIButton = {
       let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "grid"), for: .normal)
        button.addTarget(self, action: #selector(handleToGridView), for: .touchUpInside)
        return button
    }()
    
    @objc func handleToGridView() {
        listButton.tintColor = .mainColors(.inactiveButtonState)
        gridButton.tintColor = .mainColors(.activeButtonState)
        delegate?.didChangeToGridView()
    }
    
    lazy var listButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        button.tintColor = .mainColors(.inactiveButtonState)
        button.addTarget(self, action: #selector(handleToListView), for: .touchUpInside)
        return button
    }()
    
    @objc func handleToListView() {
        listButton.tintColor = .mainColors(.activeButtonState)
        gridButton.tintColor = .mainColors(.inactiveButtonState)
        delegate?.didChangeToListView()
    }
    
    let bookmarkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "ribbon"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        return button
    }()
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "username"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let postsLabel: UILabel = {
        let label = UILabel()

        let attributedText = NSMutableAttributedString()
        attributedText.appendBold(text: "11\n", size: 14)
        attributedText.appendSlim(text: "posts", size: 14)
        label.attributedText = attributedText
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let followersLabel: UILabel = {
        let label = UILabel()
        
        let attributedText = NSMutableAttributedString()
        attributedText.appendBold(text: "11\n", size: 14)
        attributedText.appendSlim(text: "followers", size: 14)
        label.attributedText = attributedText
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let followingLabel: UILabel = {
        let label = UILabel()
        
        let attributedText = NSMutableAttributedString()
        attributedText.appendBold(text: "11\n", size: 14)
        attributedText.appendSlim(text: "following", size: 14)
        label.attributedText = attributedText
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    lazy var editProfileButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleEditProfileOrFollow), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(profileImageView)
        profileImageView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 12, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 80, height: 80)
        profileImageView.layer.cornerRadius = 80 / 2
        profileImageView.clipsToBounds = true
        setupBottomToolbar()
        
        addSubview(usernameLabel)
        usernameLabel.anchor(top: profileImageView.bottomAnchor, left: leftAnchor, bottom: gridButton.topAnchor, right: rightAnchor, paddingTop: 4, paddingLeft: 12, paddingBottom: 0, paddingRight: 12, width: 0, height: 0)
        
        setupStatsView()
        
        addSubview(editProfileButton)
        editProfileButton.anchor(top: postsLabel.bottomAnchor, left: postsLabel.leftAnchor, bottom: nil, right: followingLabel.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 34)
    }
    
    fileprivate func setupStatsView() {
        let stackview = UIStackView(arrangedSubviews: [postsLabel, followersLabel, followingLabel])
        addSubview(stackview)
        stackview.anchor(top: topAnchor, left: profileImageView.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 12, paddingLeft: 12, paddingBottom: 0, paddingRight: 12, width: 0, height: 50)
        stackview.distribution = .fillEqually
        
    }
    
    fileprivate func setupBottomToolbar() {
        
        let topDividerView = UIView()
        topDividerView.backgroundColor = UIColor.lightGray
        
        let bottomDividerView = UIView()
        bottomDividerView.backgroundColor = UIColor.lightGray
        
        let stackView = UIStackView(arrangedSubviews: [listButton, gridButton, bookmarkButton])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        
        addSubview(stackView)
        addSubview(topDividerView)
        addSubview(bottomDividerView)
        stackView.anchor(top: nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)

        topDividerView.anchor(top: stackView.topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0.5)

        bottomDividerView.anchor(top: stackView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0.5)
        
    }

    fileprivate func setupProfileImage() {
        guard let user = self.user else { return }
        guard let url = URL(string: user.profileImageUrl) else { return }
        self.profileImageView.loadImage(url: url.absoluteString)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
