//
//  UserProfileController.swift
//  InstagramFirebase
//
//  Created by Ling on 20.08.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit
import Firebase

class UserProfileController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellIds = [
        "headerCell": "headerId",
        "gridCell": "gridCell",
        "listCell": "listCell"
    ]
    
    var posts = [Post]()
    var user: User?
    var isGridView = true
    
    let fetchGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = .white
        collectionView.alwaysBounceVertical = true
        collectionView?.register(UserProfileHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellIds["headerCell"]!)
        collectionView?.register(UserPostsPhotoCell.self, forCellWithReuseIdentifier: cellIds["gridCell"]!)
        collectionView.register(HomePostCell.self, forCellWithReuseIdentifier: cellIds["listCell"]!)
        
        // fetchPosts()
        setUser()
        fetchGroup.notify(queue: .main) {
            self.setupLogOutButton()
            self.fetchOrderedPosts()
        }
    }

// Fetch images one by one
    fileprivate func fetchOrderedPosts() {
        //fetchGroup.wait()
        guard let uid = user?.uid else { return }
        let ref = Database.database().reference().child("posts").child(uid)
        
        ref.queryOrdered(byChild: "creationDate").observe(.childAdded) { (snapshot) in
            
            guard let dict = snapshot.value as? [String: Any] else { return }
            guard let user = self.user else { return }
            
            let post = Post(user: user,dictionary: dict)
            self.posts.insert(post, at: 0)
            self.collectionView.reloadData()
        }
    }
    
    
//// Fetch all images at once and then iterate through with a forEach
////
//    fileprivate func fetchPosts() {
//        guard let uid = Auth.auth().currentUser?.uid else { return }
//        let ref = Database.database().reference().child("posts").child(uid)
//        ref.queryOrdered(byChild: "creationDate").observeSingleEvent(of: .value) { (snapshot) in
//            if let values = snapshot.value as? [String: Any]{
//                values.forEach({ (key, value) in
//
//                    guard let dict = value as? [String: Any] else { return }
//                    guard let user = self.user else { return }
//
//                    let post = Post(user: user,dictionary: dict)
//                    self.posts.append(post)
//                })
//                self.collectionView?.reloadData()
//            }
//        }
//    }
//
    

    fileprivate func setupLogOutButton() {
        guard let loggedInUserUid = Auth.auth().currentUser?.uid else { return }
        guard let userId = user?.uid else { return }
        
        if loggedInUserUid == userId {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "gear").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleLogOut))
        }
    }
    
    @objc func handleLogOut() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Log out", style: .destructive, handler: { (_) in
            print("Perform log out")
            do {
                try Auth.auth().signOut()
                let loginController = LoginController()
                let navigationController = UINavigationController(rootViewController: loginController)
                self.present(navigationController, animated: true, completion: nil)
                
            } catch let signOutErr {
                print("Failed to sign out:", signOutErr)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! UserProfileHeader
        if let user = self.user {
            header.user = user
        }
        header.delegate = self
        return header
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isGridView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIds["gridCell"]!, for: indexPath) as! UserPostsPhotoCell
            cell.post = self.posts[indexPath.row]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIds["listCell"]!, for: indexPath) as! HomePostCell
            cell.post = self.posts[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isGridView {
            let width = (view.frame.width - 3) / 3
            return CGSize(width: width, height: width)
        } else {
            return CGSize(width: view.frame.width, height: view.frame.width)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 200)
    }
    
    
    fileprivate func setUser() {
        guard let user = self.user else {
            fetchUser()
            return
        }
        self.navigationItem.title = user.username
        self.collectionView?.reloadData()
    }
    
    fileprivate func fetchUser() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        fetchGroup.enter()
        Database.fetchUserWithUid(uid: uid) { (user) -> (Void) in
            self.user = user
            self.navigationItem.title = self.user?.username
            self.fetchGroup.leave()
        }
    }
    
}

extension UserProfileController: UserProfileHeaderDelegate {
    func didChangeToListView() {
        isGridView = false
        collectionView.reloadData()
    }
    
    func didChangeToGridView() {
        isGridView = true
        collectionView.reloadData()
    }
}
