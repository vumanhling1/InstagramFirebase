//
//  Posts.swift
//  InstagramFirebase
//
//  Created by Ling on 14.09.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import Foundation

struct Post {
    
    var id: String?
    
    var user: User
    var imageUrl: String
    var caption: String
    let creationDate: Date
    var hasLiked: Bool = true
    
    
    init(user: User, dictionary: [String: Any]) {
        self.user = user
        self.imageUrl = dictionary["imageUrl"] as? String ?? ""
        self.caption = dictionary["caption"] as? String ?? ""
        
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)        
    }
}
