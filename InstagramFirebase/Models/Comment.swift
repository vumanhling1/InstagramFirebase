//
//  Comment.swift
//  InstagramFirebase
//
//  Created by Ling on 25.10.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import Foundation
import Firebase

struct Comment {
    let text: String
    var user: User
    
    init(user: User, dictionary: [String: Any]) {
        self.text = dictionary["text"] as? String ?? ""
        self.user = user
    }
}
