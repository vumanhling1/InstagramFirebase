//
//  HomePostCell.swift
//  InstagramFirebase
//
//  Created by Ling on 16.09.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit

protocol HomePostCellDelegate {
    func didTapComment(post: Post)
    func didLike(for post: HomePostCell)
}

class HomePostCell: UICollectionViewCell {
    
    var delegate: HomePostCellDelegate?
    
    var post: Post? {
        didSet {
            guard let imageUrl = post?.imageUrl else { return }
            photoImageView.loadImage(url: imageUrl)
            usernameLabel.text = post?.user.username
            
            likeButton.setImage(post?.hasLiked == false ? #imageLiteral(resourceName: "like_unselected") : #imageLiteral(resourceName: "like_selected"), for: .normal)
            likeButton.tintColor = .white
            
            guard let profileImageUrl = post?.user.profileImageUrl else { return }
            userProfileImageView.loadImage(url: profileImageUrl)
            
            guard let captionText = post?.caption else { return }
            setAttributedCaptionText(text: captionText)
            setAddedDateLabelText()
        }
    }
    
    fileprivate func setAddedDateLabelText() {
        guard let timeAgoDisplay = post?.creationDate.timeAgoDisplay() else { return }
        addedDateLabel.text = timeAgoDisplay
    }
    
    fileprivate func setAttributedCaptionText(text: String) {
        let attributedText = NSMutableAttributedString()
        attributedText.appendBold(text: text, size: 14, color: UIColor.white)
        
        captionLabel.attributedText = attributedText
    }
    
    let userProfileImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "Username"
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let addedDateLabel: UILabel = {
        let label = UILabel()
        label.text = "2 Hours"
        label.textColor = UIColor(white: 0.9, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    let optionsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("•••", for: .normal)
        button.setTitleColor(.white, for: .normal)
        return button
    }()

    lazy var likeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "like_selected"), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleLike), for: .touchUpInside)
        return button
    }()
    
    @objc func handleLike() {
        delegate?.didLike(for: self)
    }
    
    lazy var commentButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "comment"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        return button
    }()
    
    @objc func handleComment() {
        print("Trying to show comments")
        guard let post = post else { return }
        delegate?.didTapComment(post: post)
    }
    
    let sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "send2"), for: .normal)
        button.tintColor = .white
        return button
    }()
    
    let bookmarkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "ribbon"), for: .normal)
        button.tintColor = .white
        return button
    }()
    
    let userPostBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.1, alpha: 0.8)
        view.clipsToBounds = true
        return view
    }()
    
    let photoImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        
        iv.layer.cornerRadius = UIScreen.main.bounds.width / 30
        iv.layer.masksToBounds = true
        return iv
    }()
    
    let captionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(photoImageView)
        photoImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 0)
        
        photoImageView.addSubview(userPostBackgroundView)
        userPostBackgroundView.anchor(top: nil, left: photoImageView.leftAnchor, bottom: photoImageView.bottomAnchor, right: photoImageView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 56)
        
        photoImageView.addSubview(userProfileImageView)
        userProfileImageView.layer.cornerRadius = 40 / 2
        userProfileImageView.clipsToBounds = true
        userProfileImageView.anchor(top: userPostBackgroundView.topAnchor, left: photoImageView.leftAnchor, bottom: userPostBackgroundView.bottomAnchor, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 0, width: 40, height: 40)
        
        photoImageView.addSubview(usernameLabel)
        usernameLabel.anchor(top: userProfileImageView.topAnchor, left: userProfileImageView.rightAnchor, bottom: nil, right: photoImageView.rightAnchor, paddingTop: 2, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 0)
        
        photoImageView.addSubview(addedDateLabel)
        addedDateLabel.anchor(top: nil, left: userProfileImageView.rightAnchor, bottom: userProfileImageView.bottomAnchor, right: photoImageView.rightAnchor, paddingTop: 0, paddingLeft: 8, paddingBottom: 2, paddingRight: 8, width: 0, height: 0)
        
        setupActionButtons()
        setupCaption()
    }
    
    fileprivate func setupActionButtons() {
        let stackView = UIStackView(arrangedSubviews: [likeButton, commentButton, sendButton, optionsButton])
        
        stackView.distribution = .fillEqually
        addSubview(stackView)
        stackView.anchor(top: userPostBackgroundView.topAnchor, left: nil, bottom: userPostBackgroundView.bottomAnchor, right: userPostBackgroundView.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 8, paddingRight: 8, width: 150, height: 40)
        
        addSubview(bookmarkButton)
        bookmarkButton.anchor(top: photoImageView.topAnchor, left: nil, bottom: nil, right: photoImageView.rightAnchor, paddingTop: 8, paddingLeft: 0, paddingBottom: 0, paddingRight: 8, width: 40, height: 40)
    }
    
    fileprivate func setupCaption() {
        addSubview(captionLabel)
        captionLabel.anchor(top: photoImageView.topAnchor, left: photoImageView.leftAnchor, bottom: nil, right: photoImageView.rightAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 50, width: 0, height: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
