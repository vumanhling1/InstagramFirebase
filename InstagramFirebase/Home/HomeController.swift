//
//  HomeController.swift
//  InstagramFirebase
//
//  Created by Ling on 16.09.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit
import Firebase

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout, HomePostCellDelegate {
    
    let FOLLOWING = 1
    
    let cellId = "cellId"
    var loadingPosts = [Post]()
    var showingPosts = [Post]()
    
    let fetchPostsGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: UPDATE_FEED_NOTIFICATION, object: nil)
        
        collectionView?.backgroundColor = .white
        collectionView?.register(HomePostCell.self, forCellWithReuseIdentifier: cellId)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        
        setupNavigationItems()
        
        fetchAllPosts()
    }
    
    @objc func handleUpdateFeed() {
        handleRefresh()
    }
    
    @objc func handleRefresh() {
        loadingPosts.removeAll()
        fetchAllPosts()
    }
    
    fileprivate func fetchAllPosts() {
        fetchPosts()
        fetchFollowingUserIds()
        
        fetchPostsGroup.notify(queue: .main, execute: {
            self.showingPosts = self.loadingPosts
            self.collectionView.reloadData()
            self.collectionView.refreshControl?.endRefreshing()
        })
    }
    
    fileprivate func fetchFollowingUserIds() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        Database.database().reference().child("following").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let userIdsDictionary = snapshot.value as? [String: Any] else { return }
            
            userIdsDictionary.forEach({ (uid, value) in
                if let status = value as? Int, status == self.FOLLOWING {
                    self.fetchPostsGroup.enter()
                    Database.fetchUserWithUid(uid: uid, completion: { (user) -> (Void) in
                        self.fetchPostsWithUser(user: user)
                    })
                }
            })
        }) { (err) in
            print("Failed to fetch following user ids:", err)
        }
    }
    
    
    fileprivate func fetchPosts() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        self.fetchPostsGroup.enter()
        Database.fetchUserWithUid(uid: uid) { (user) in
            self.fetchPostsWithUser(user: user)
        }
    }
    
    fileprivate func fetchPostsWithUser(user: User) {
        let ref = Database.database().reference().child("posts").child(user.uid)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let dicts = snapshot.value as? [String: Any] else {
                self.fetchPostsGroup.leave()
                return
            }
            
            dicts.forEach({ (key, value) in
                guard let dict = value as? [String: Any] else { return }
                var post = Post(user: user, dictionary: dict)
                post.id = key
                
                self.fetchPostsGroup.enter()
                
                guard let uid = Auth.auth().currentUser?.uid else { return }
                Database.database().reference().child("likes").child(key).child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    if let value = snapshot.value as? Int, value == 1 {
                        post.hasLiked = true
                    } else {
                        post.hasLiked = false
                    }
                    
                    self.loadingPosts.append(post)
                    self.loadingPosts.sort(by: { (p1, p2) -> Bool in
                        return p1.creationDate.compare(p2.creationDate) == .orderedDescending
                    })
                    self.fetchPostsGroup.leave()
                }, withCancel: { (err) in
                    print("Failed to fetch like info for post: ", err)
                    self.fetchPostsGroup.leave()
                })
            })
            
            self.fetchPostsGroup.leave()
        }) { (err) in
            print("Failed to fetch posts:", err)
        }
        
    }
    
    func setupNavigationItems() {
        navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "logo2"))
    
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "camera3").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleCamera))
    }
    
    @objc func handleCamera() {
        let cameraController = CameraController()
        present(cameraController, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = view.frame.width + 80
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return showingPosts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? HomePostCell {
            cell.post = showingPosts[indexPath.item]
            cell.delegate = self
            return cell
        }
 
        return UICollectionViewCell()
    }
    
    func didTapComment(post: Post) {
        print("Message coming from HomeController")
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.hidesBottomBarWhenPushed = true
        commentsController.post = post
        navigationController?.pushViewController(commentsController, animated: true)
    }
    
    func didLike(for cell: HomePostCell) {
        
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        var post = showingPosts[indexPath.item]
        guard let postId = post.id else { return }
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let values = [uid: post.hasLiked == true ? 1 : 0]
        
        Database.database().reference().child("likes").child(postId).updateChildValues(values) { (err, _) in
            if let err = err {
                print("Failed to like post: ", err)
            }
            
            print("Successfully liked post: ", post.caption)
            
            post.hasLiked = !post.hasLiked
            self.showingPosts[indexPath.item] = post
            self.collectionView.reloadItems(at: [indexPath])
        }
    }
}
