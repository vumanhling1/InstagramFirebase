//
//  PreviewPhotoContainerView.swift
//  InstagramFirebase
//
//  Created by Ling on 21.10.18.
//  Copyright © 2018 Ling. All rights reserved.
//

import UIKit
import Photos

class PreviewPhotoContainerView: UIView {
    
    let previewImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "cancel_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleCancel() {
        self.removeFromSuperview()
    }
    
    let saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "save_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleSave() {
        checkPhotoLibraryPermission()
        
        let library = PHPhotoLibrary.shared()
        
        guard let previewImage = previewImageView.image else { return }
        
        library.performChanges({
            PHAssetChangeRequest.creationRequestForAsset(from: previewImage)
        }) { (success, err) in
            if let err = err {
                print("Failed to save image to photo library:", err)
            }
            
            print("Successfully saved image to library")
            
            DispatchQueue.main.async {
                self.showSavedMessage()
            }
        }
    }
    
    func showSavedMessage() {
        let savedLabel = UILabel()
        savedLabel.text = "Saved Successfully"
        savedLabel.textColor = .white
        savedLabel.numberOfLines = 0
        savedLabel.textAlignment = .center
        savedLabel.backgroundColor = UIColor(white: 0, alpha: 0.3)
        
        savedLabel.frame = CGRect(x: 0, y: 0, width: 160, height: 80)
        savedLabel.center = self.center
        
        self.addSubview(savedLabel)
        
        savedLabel.layer.transform = CATransform3DMakeScale(0, 0, 0)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            // FADE IN
            savedLabel.layer.transform = CATransform3DMakeScale(1, 1, 1)
        }, completion: { (completed) in
            UIView.animate(withDuration: 0.5, delay: 0.75, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                // FADE OUT
                savedLabel.layer.transform = CATransform3DMakeScale(0.1, 0.1, 0.1)
                savedLabel.alpha = 0
            }, completion: { (completed) in
                savedLabel.removeFromSuperview()
            })
        })
    }
    
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            return
        case .denied, .restricted :
            return
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    return
                case .denied, .restricted:
                    return
                case .notDetermined:
                    return
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        addSubview(previewImageView)
        previewImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        addSubview(cancelButton)
        cancelButton.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 24, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        addSubview(saveButton)
        saveButton.anchor(top: nil, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 12, paddingBottom: 24, paddingRight: 0, width: 50, height: 50)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
